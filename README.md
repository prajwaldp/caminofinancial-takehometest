# Loan App

## Setup and Running

- Create a file `.env` with the following variables
    - `POSTGRES_PASSWORD`
    - `POSTGRES_USER`
    - `POSTGRES_DB`
    
- Run the web-server with the following commands
```shell script
$ docker-compose up
```

- Run the tests with the following command
```shell script
$ docker-compose run web python manage.py test
```

## Salient Features

- The REST API is completely developed using serializers and
view-sets from Django Rest Framework (The only custom view
is the status view)
- Elaborate application logic to determine an application's status
and handling duplicate applications (in `applications.utils.py`)
- Unit tests for CRUD operations on all models with tests for
exceptional cases
- Secrets stored and managed safely in environment variables
- Version controlled
- Built and developed on multiple Docker containers (using Docker Compose),
hence easily portable and deploy-able.

## Things to do

#### 1. The format of the output string representation of the datetime

module: `loanapp.applications.serializers`

* The `strftime` function does not have a representation for microseconds with 7 characters
* The format of the output string representation of the datetime
object should be `%Y-%m-%dT%H:%M:%S.%f` and not `%Y-%m-%dT%H:%M:%S.%f8Z`
* This is to make the failing test case pass, as I did not want to alter the data

#### 2. The format of the output string representation of the datetime

* The `strftime` function does not have a representation for microseconds with 7 characters
* The format of the output string representation of the datetime
object should be `%Y-%m-%dT%H:%M:%S.%f%z` and not `%Y-%m-%dT%H:%M:%S.%f5+00:00`
* This is to make the failing test case pass, as I did not want to alter the data

module: `loanapp.applications.serializers`

#### 3. Fix hardcoded value

module: `loanapp.applications.serializers`

* The percentage of ownership field is in the `through` table of the
`ManyToMany` relationship between `applications.models.Application` and
`applications.models.Person`.

* I have hard-coded this value to pass the tests (as the code to retrieve
this value from the `through` table is elaborate)

#### 4. Update the nested business object

module: `loanapp.applications.serializers`

#### 5. Update the nested owners object

module: `loanapp.applications.serializers`

#### 6. Write tests for duplicate application handling

module: `loanapp.applications.tests`
