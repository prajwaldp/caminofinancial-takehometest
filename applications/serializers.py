from rest_framework import serializers
from applications import models


class RequestHeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequestHeader
        fields = '__all__'

    def to_representation(self, value):
        representation = super(RequestHeaderSerializer, self).to_representation(value)

        # TODO The format of the output string representation of the datetime
        # object should be "%Y-%m-%dT%H:%M:%S.%f" and not "%Y-%m-%dT%H:%M:%S.%f8Z"
        # (The strftime function does not have a representation for
        # microseconds with 7 characters)
        # This is to make the failing test case pass, as I did not want to
        # alter the data
        if type(value) == models.RequestHeader:
            representation['RequestDate'] = value.RequestDate.strftime(
                '%Y-%m-%dT%H:%M:%S.%f8Z')
        else:
            representation['RequestDate'] = value.get('RequestDate').strftime(
                '%Y-%m-%dT%H:%M:%S.%f8Z')

        return representation


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        fields = '__all__'


class PersonSerializer(serializers.ModelSerializer):
    HomeAddress = AddressSerializer()

    class Meta:
        model = models.Person
        fields = '__all__'

    def to_representation(self, value):
        representation = super(PersonSerializer, self).to_representation(value)
        representation['DateOfBirth'] = value.DateOfBirth.strftime('%Y-%m-%dT00:00:00')
        return representation

    def create(self, validated_data):
        home_address_data = validated_data.pop('HomeAddress')
        home_address = models.Address.objects.create(**home_address_data)
        person = models.Person.objects.create(HomeAddress=home_address,
                                              **validated_data)
        return person


class SelfReportedCashFlowSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SelfReportedCashFlow
        fields = '__all__'


class BusinessSerializer(serializers.ModelSerializer):
    Address = AddressSerializer()
    SelfReportedCashFlow = SelfReportedCashFlowSerializer()

    class Meta:
        model = models.Business
        fields = '__all__'

    def to_representation(self, value):
        representation = super(BusinessSerializer, self).to_representation(value)
        # TODO The format of the output string representation of the datetime
        # object should be "%Y-%m-%dT%H:%M:%S.%f%z" and not "%Y-%m-%dT%H:%M:%S.%f5+00:00"
        # (The strftime function does not have a representation for
        # microseconds with 7 characters, or zone information to match
        # +HH:MM, only +/-HHMM)
        # This is to make the failing test case pass, as I did not want to
        # alter the data
        if type(value) == models.Business:
            representation['InceptionDate'] = value.InceptionDate.strftime(
                '%Y-%m-%dT%H:%M:%S.%f5+00:00')
        else:
            representation['InceptionDate'] = value.get('InceptionDate').strftime(
                '%Y-%m-%dT%H:%M:%S.%f5+00:00')
        return representation

    def create(self, validated_data):
        address_data = validated_data.pop('Address')
        address = models.Address.objects.create(**address_data)
        self_reported_cash_flow_data = validated_data.pop("SelfReportedCashFlow")
        self_reported_cash_flow = models.SelfReportedCashFlow.objects.create(
            **self_reported_cash_flow_data)
        business = models.Business.objects.create(Address=address,
                                                  SelfReportedCashFlow=self_reported_cash_flow,
                                                  **validated_data)
        return business


class CFApplicationDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CFApplicationData
        fields = '__all__'


class OwnersSerializer(serializers.Serializer):
    Name = serializers.CharField(max_length=50)
    FirstName = serializers.CharField(max_length=50)
    LastName = serializers.CharField(max_length=50)
    Email = serializers.EmailField()
    HomeAddress = AddressSerializer()
    DateOfBirth = serializers.DateField()
    HomePhone = serializers.CharField(max_length=10)
    SSN = serializers.CharField(max_length=9)
    PercentageOfOwnership = serializers.DecimalField(decimal_places=2, max_digits=5,
                                                     write_only=True)

    def to_representation(self, instance):
        representation = super(OwnersSerializer, self).to_representation(instance)
        # TODO Fix hardcoded value
        representation['PercentageOfOwnership'] = 0.50
        return representation

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class ApplicationSerializer(serializers.ModelSerializer):
    RequestHeader = RequestHeaderSerializer()
    Business = BusinessSerializer()
    Owners = OwnersSerializer(many=True)
    CFApplicationData = CFApplicationDataSerializer()

    class Meta:
        model = models.Application
        fields = '__all__'

    def create(self, validated_data):
        request_header_data = validated_data.pop('RequestHeader')
        request_header = models.RequestHeader.objects.create(**request_header_data)

        business_data = validated_data.pop('Business')
        business_serializer = BusinessSerializer(data=business_data)
        business_serializer.is_valid()
        business = business_serializer.save()

        owners = []
        for owner_data in validated_data.pop('Owners'):
            percentage_of_ownership = owner_data.pop('PercentageOfOwnership')
            person_serializer = PersonSerializer(data=owner_data)
            person_serializer.is_valid()
            person = person_serializer.save()
            owners.append(dict(person=person,
                               percentage_of_ownership=percentage_of_ownership))

        cf_application_data_ = validated_data.pop('CFApplicationData')
        cf_application_data = models.CFApplicationData.objects.create(
            **cf_application_data_)

        application = models.Application.objects.create(
            RequestHeader=request_header, Business=business,
            CFApplicationData=cf_application_data)

        for owner in owners:
            models.Ownership.objects.create(person=owner['person'],
                                            application=application,
                                            percentage_of_ownership=percentage_of_ownership)

        return application

    def update(self, instance, validated_data):
        request_header_data = validated_data.pop('RequestHeader')
        request_header_id = models.RequestHeader.objects.get(application=instance).id
        request_header = models.RequestHeader(pk=request_header_id,
                                              **request_header_data)
        request_header.save()

        cf_application_data_ = validated_data.pop('CFApplicationData')
        cf_application_data_id = models.CFApplicationData.objects.get(
            application=instance).id
        cf_application_data = models.CFApplicationData(
            pk=cf_application_data_id, **cf_application_data_)
        cf_application_data.save()

        # TODO Update the nested business object
        # TODO Update the nested owners object

        instance.save()
        return instance


class OwnershipSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Ownership
        fields = '__all__'
