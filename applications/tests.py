import json
from django.test import TestCase
from rest_framework.test import APIClient
from applications import models, serializers


class TestHelper:
    @classmethod
    def all_fields_equal(cls, obj1, obj2, fields=[]):
        for field in fields:
            if obj1[field] != obj2[field]:
                print(field)
                print(obj1[field])
                print(obj2[field])
        return all(obj1[field] == obj2[field] for field in fields)


class ApplicationsTestCase(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()

    def test_can_create_addresses(self) -> None:
        """
        Tests that a POST request to /addresses creates
        the Address instance and adds it to the database
        """
        payload = """{
            "Address1": "5567 North Ridge Ct",
            "Address2": null,
            "City": "Berkeley",
            "State": "CA",
            "Zip": "94704"
        }"""
        parsed_payload = json.loads(payload)
        resp = self.client.post('/addresses/', payload,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)

        # Get addresses stored in the database
        addresses = models.Address.objects.all()
        self.assertEqual(len(addresses), 1)
        serialized = serializers.AddressSerializer(addresses[0]).data

        fields = ['Address1', 'Address2', 'City', 'State', 'Zip']
        self.assert_(TestHelper.all_fields_equal(serialized, parsed_payload, fields))

    def test_can_create_persons(self) -> None:
        """
        Tests that a POST request to /persons with nested address data
        creates the Person instance and adds it to the database
        """
        payload = """{
            "Name": "Test DoeTest",
            "FirstName": "Test",
            "LastName": "DoeTest",
            "Email": "Doetest@caminofinancial.com",
            "HomeAddress": {
                "Address1": "4512 East Ridge Ct",
                "Address2": null,
                "City": "Berkeley",
                "State": "CA",
                "Zip": "94704"
            },
            "DateOfBirth": "1955-12-18T00:00:00",
            "HomePhone": "3107654321",
            "SSN": "435790261"
        }"""
        parsed_payload = json.loads(payload)
        resp = self.client.post('/persons/', payload,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)

        # Get persons and addresses stored in the database
        persons = models.Person.objects.all()
        self.assertEqual(len(persons), 1)
        addresses = models.Address.objects.all()
        self.assertEqual(len(addresses), 1)
        serialized = serializers.PersonSerializer(persons[0]).data

        person_fields = ['Name', 'FirstName', 'LastName', 'Email',
                         'DateOfBirth', 'HomePhone', 'SSN']
        address_fields = ['Address1', 'Address2', 'City', 'State', 'Zip']

        self.assert_(
            TestHelper.all_fields_equal(serialized,
                                        parsed_payload,
                                        person_fields))

        self.assert_(
            TestHelper.all_fields_equal(serialized['HomeAddress'],
                                        parsed_payload['HomeAddress'],
                                        address_fields))

    def test_can_create_businesses(self) -> None:
        """
        Tests that a POST request to /businesses with nested data
        creates the Business instance and adds it to the database
        """
        payload = """{
            "Name":"Wow Inc",
            "SelfReportedCashFlow":{
                "AnnualRevenue":49999999.0,
                "MonthlyAverageBankBalance":94941.0,
                "MonthlyAverageCreditCardVolume":18191.0
            },
            "Address":{
                "Address1":"1234 Red Ln",
                "Address2":"5678 Blue Rd",
                "City":"Santa Monica",
                "State":"CA",
                "Zip":"45321"
            },
            "TaxID":"839674398",
            "Phone":"6573248876",
            "NAICS":"79232",
            "HasBeenProfitable":true,
            "HasBankruptedInLast7Years":false,
            "InceptionDate":"2008-06-28T23:04:03.5507585+00:00"
        }"""

        parsed_payload = json.loads(payload)
        resp = self.client.post('/businesses/', payload,
                                content_type='application/json')
        self.assertEqual(resp.status_code, 201)

        businesses = models.Business.objects.all()
        self.assertEqual(len(businesses), 1)
        addresses = models.Address.objects.all()
        self.assertEqual(len(addresses), 1)

        serialized = serializers.BusinessSerializer(businesses[0]).data

        business_fields = ['Name', 'TaxID', 'Phone', 'NAICS',
                           'HasBeenProfitable', 'HasBankruptedInLast7Years',
                           'InceptionDate']
        address_fields = ['Address1', 'Address2', 'City', 'State', 'Zip']
        self_reported_cash_flow_fields = ['AnnualRevenue',
                                          'MonthlyAverageBankBalance',
                                          'MonthlyAverageCreditCardVolume']

        self.assert_(
            TestHelper.all_fields_equal(serialized,
                                        parsed_payload,
                                        business_fields))

        self.assert_(
            TestHelper.all_fields_equal(serialized['Address'],
                                        parsed_payload['Address'],
                                        address_fields))

        self.assert_(
            TestHelper.all_fields_equal(serialized['SelfReportedCashFlow'],
                                        parsed_payload['SelfReportedCashFlow'],
                                        self_reported_cash_flow_fields))

    def test_can_create_loan_applications(self) -> None:
        """
        Tests that a POST request to /applications with nested data
        creates the Application instance and adds it to the database
        """
        payload = """{
            "RequestHeader":{
                "CFRequestId":"500653901",
                "RequestDate":"2019-06-26T23:05:41.2898238Z",
                "CFApiUserId":null,
                "CFApiPassword":null,
                "IsTestLead":true
            },
            "Business":{
                "Name":"Wow Inc",
                "SelfReportedCashFlow":{
                    "AnnualRevenue":49999999.0,
                    "MonthlyAverageBankBalance":94941.0,
                    "MonthlyAverageCreditCardVolume":18191.0
                },
                "Address":{
                    "Address1":"1234 Red Ln",
                    "Address2":"5678 Blue Rd",
                    "City":"Santa Monica",
                    "State":"CA",
                    "Zip":"45321"
                },
                "TaxID":"839674398",
                "Phone":"6573248876",
                "NAICS":"79232",
                "HasBeenProfitable":true,
                "HasBankruptedInLast7Years":false,
                "InceptionDate":"2008-06-28T23:04:03.5507585+00:00"
            },
            "Owners":[
                {
                    "Name":"WH KennyTest",
                    "FirstName":"WH",
                    "LastName":"KennyTest",
                    "Email":"whkennytest@caminofinancial.com",
                    "HomeAddress":{
                        "Address1":"5567 North Ridge Ct",
                        "Address2":null,
                        "City":"Berkeley",
                        "State":"CA",
                        "Zip":"94704"
                    },
                    "DateOfBirth":"1955-12-18T00:00:00",
                    "HomePhone":"3451289776",
                    "SSN":"435790261",
                    "PercentageOfOwnership":50.0
                },
                {
                    "Name":"Test DoeTest",
                    "FirstName":"Test",
                    "LastName":"DoeTest",
                    "Email":"Doetest@caminofinancial.com",
                    "HomeAddress":{
                        "Address1":"4512 East Ridge Ct",
                        "Address2":null,
                        "City":"Berkeley",
                        "State":"CA",
                        "Zip":"94704"
                    },
                    "DateOfBirth":"1955-12-18T00:00:00",
                    "HomePhone":"3107654321",
                    "SSN":"435790261",
                    "PercentageOfOwnership":50.0
                },
                {
                    "Name":"Test DoeTest",
                    "FirstName":"Test",
                    "LastName":"DoeTest",
                    "Email":"Doetest@caminofinancial.com",
                    "HomeAddress":{
                        "Address1":"4512 East Ridge Ct",
                        "Address2":null,
                        "City":"Berkeley",
                        "State":"CA",
                        "Zip":"94704"
                    },
                    "DateOfBirth":"1955-12-18T00:00:00",
                    "HomePhone":"3107654321",
                    "SSN":"435790261",
                    "PercentageOfOwnership":50.0
                },
                {
                    "Name":"Test DoeTest",
                    "FirstName":"Test",
                    "LastName":"DoeTest",
                    "Email":"Doetest@caminofinancial.com",
                    "HomeAddress":{
                        "Address1":"4512 East Ridge Ct",
                        "Address2":null,
                        "City":"Berkeley",
                        "State":"CA",
                        "Zip":"94704"
                    },
                    "DateOfBirth":"1955-12-18T00:00:00",
                    "HomePhone":"3107654321",
                    "SSN":"435790261",
                    "PercentageOfOwnership":50.0
                },
                {
                    "Name":"Test DoeTest",
                    "FirstName":"Test",
                    "LastName":"DoeTest",
                    "Email":"Doetest@caminofinancial.com",
                    "HomeAddress":{
                        "Address1":"4512 East Ridge Ct",
                        "Address2":null,
                        "City":"Berkeley",
                        "State":"CA",
                        "Zip":"94704"
                    },
                    "DateOfBirth":"1955-12-18T00:00:00",
                    "HomePhone":"3107654321",
                    "SSN":"435790261",
                    "PercentageOfOwnership":50.0
                }
            ],
            "CFApplicationData":{
                "RequestedLoanAmount":"49999999",
                "StatedCreditHistory":1,
                "LegalEntityType":"LLC",
                "FilterID":"897079"
            }
        }"""

        parsed_payload = json.loads(payload)
        resp = self.client.post('/loanapp/', payload, content_type='application/json')

        self.assertEqual(resp.status_code, 201)

        applications = models.Application.objects.all()
        self.assertEqual(len(applications), 1)

        businesses = models.Business.objects.all()
        self.assertEqual(len(businesses), 1)

        addresses = models.Address.objects.all()
        self.assertEqual(len(addresses), 6)

        persons = models.Person.objects.all()
        self.assertEqual(len(persons), 5)

        serialized = serializers.ApplicationSerializer(applications[0]).data

        request_header_fields = ['CFRequestId', 'RequestDate', 'CFApiUserId',
                                 'CFApiPassword', 'IsTestLead']
        self.assert_(
            TestHelper.all_fields_equal(serialized['RequestHeader'],
                                        parsed_payload['RequestHeader'],
                                        request_header_fields))

        cf_application_data_fields = ['RequestedLoanAmount', 'StatedCreditHistory',
                                      'LegalEntityType', 'FilterID']
        self.assert_(
            TestHelper.all_fields_equal(serialized['CFApplicationData'],
                                        parsed_payload['CFApplicationData'],
                                        cf_application_data_fields))

        business_fields = ['Name', 'TaxID', 'Phone', 'NAICS',
                           'HasBeenProfitable', 'HasBankruptedInLast7Years',
                           'InceptionDate']
        self.assert_(
            TestHelper.all_fields_equal(serialized['Business'],
                                        parsed_payload['Business'],
                                        business_fields))

        self_reported_cash_flow_fields = ['AnnualRevenue',
                                          'MonthlyAverageBankBalance',
                                          'MonthlyAverageCreditCardVolume']

        self.assert_(
            TestHelper.all_fields_equal(serialized['Business']['SelfReportedCashFlow'],
                                        parsed_payload['Business']['SelfReportedCashFlow'],
                                        self_reported_cash_flow_fields))

        # Create a new Person
        payload = """{
            "Name": "Test DoeTest2",
            "FirstName": "Test",
            "LastName": "DoeTest",
            "Email": "Doetest@caminofinancial.com",
            "HomeAddress": {
                "Address1": "4512 East Ridge Ct",
                "Address2": null,
                "City": "Berkeley",
                "State": "CA",
                "Zip": "94704"
            },
            "DateOfBirth": "1955-12-18T00:00:00",
            "HomePhone": "3107654321",
            "SSN": "435790261"
        }"""
        self.client.post('/persons/', payload, content_type='application/json')

        # Test that the ownership table is constructed correctly
        owners = list(map(lambda x: x['Name'], serialized['Owners']))
        self.assert_('WH KennyTest' in owners)
        self.assert_('Test DoeTest' in owners)
        self.assert_('Test DoeTest2' not in owners)

    def test_get_application_status_returns_404_for_nonexistent_ids(self) -> None:
        """
        Tests that the /status/<application_id> endpoint returns
        404 if the application_id does not exist in the database
        """
        resp = self.client.get('/status/123')
        self.assertEqual(resp.status_code, 404)

    def test_get_application_status_returns_correctly(self) -> None:
        """
        Tests the return value of various applications based on the
        programmed logic
        """

        # Case 1 : Everything's alright
        payload = """{
            "RequestHeader":{
                "CFRequestId":"500653901",
                "RequestDate":"2019-06-26T23:05:41.2898238Z",
                "CFApiUserId":null,
                "CFApiPassword":null,
                "IsTestLead":true
            },
            "Business":{
                "Name":"Wow Inc",
                "SelfReportedCashFlow":{
                    "AnnualRevenue":49999999.0,
                    "MonthlyAverageBankBalance":94941.0,
                    "MonthlyAverageCreditCardVolume":18191.0
                },
                "Address":{
                    "Address1":"1234 Red Ln",
                    "Address2":"5678 Blue Rd",
                    "City":"Santa Monica",
                    "State":"CA",
                    "Zip":"45321"
                },
                "TaxID":"839674398",
                "Phone":"6573248876",
                "NAICS":"79232",
                "HasBeenProfitable":true,
                "HasBankruptedInLast7Years":false,
                "InceptionDate":"2008-06-28T23:04:03.5507585+00:00"
            },
            "Owners":[],
            "CFApplicationData":{
                "RequestedLoanAmount":"49999999",
                "StatedCreditHistory":1,
                "LegalEntityType":"LLC",
                "FilterID":"897079"
            }
        }"""

        resp = self.client.post('/loanapp/', payload, content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        resp = self.client.get('/status/{}'.format(json.loads(resp.content).get('id')))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)['status'], 'pending')

        # Case 2: Has been bankrupt in the last 7 years
        payload = """{
            "RequestHeader":{
                "CFRequestId":"500653901",
                "RequestDate":"2019-06-26T23:05:41.2898238Z",
                "CFApiUserId":null,
                "CFApiPassword":null,
                "IsTestLead":true
            },
            "Business":{
                "Name":"Wow Inc",
                "SelfReportedCashFlow":{
                    "AnnualRevenue":49999999.0,
                    "MonthlyAverageBankBalance":94941.0,
                    "MonthlyAverageCreditCardVolume":18191.0
                },
                "Address":{
                    "Address1":"1234 Red Ln",
                    "Address2":"5678 Blue Rd",
                    "City":"Santa Monica",
                    "State":"CA",
                    "Zip":"45321"
                },
                "TaxID":"839674398",
                "Phone":"6573248876",
                "NAICS":"79232",
                "HasBeenProfitable":true,
                "HasBankruptedInLast7Years":true,
                "InceptionDate":"2008-06-28T23:04:03.5507585+00:00"
            },
            "Owners":[],
            "CFApplicationData":{
                "RequestedLoanAmount":"49999998",
                "StatedCreditHistory":1,
                "LegalEntityType":"LLC",
                "FilterID":"897079"
            }
        }"""

        resp = self.client.post('/loanapp/', payload, content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        resp = self.client.get('/status/{}'.format(json.loads(resp.content)['id']))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)['status'], 'rejected')

        # Case 2: Has not been profitable
        payload = """{
            "RequestHeader":{
                "CFRequestId":"500653901",
                "RequestDate":"2019-06-26T23:05:41.2898238Z",
                "CFApiUserId":null,
                "CFApiPassword":null,
                "IsTestLead":true
            },
            "Business":{
                "Name":"Wow Inc",
                "SelfReportedCashFlow":{
                    "AnnualRevenue":49999999.0,
                    "MonthlyAverageBankBalance":94941.0,
                    "MonthlyAverageCreditCardVolume":18191.0
                },
                "Address":{
                    "Address1":"1234 Red Ln",
                    "Address2":"5678 Blue Rd",
                    "City":"Santa Monica",
                    "State":"CA",
                    "Zip":"45321"
                },
                "TaxID":"839674398",
                "Phone":"6573248876",
                "NAICS":"79232",
                "HasBeenProfitable":false,
                "HasBankruptedInLast7Years":false,
                "InceptionDate":"2008-06-28T23:04:03.5507585+00:00"
            },
            "Owners":[],
            "CFApplicationData":{
                "RequestedLoanAmount":"49999997",
                "StatedCreditHistory":1,
                "LegalEntityType":"LLC",
                "FilterID":"897079"
            }
        }"""
        resp = self.client.post('/loanapp/', payload, content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        resp = self.client.get('/status/{}'.format(json.loads(resp.content)['id']))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)['status'], 'rejected')

        # Case 2: Average monthly balance is less than 0.1% of the
        # requested loan amount
        # (99 is less than 10% of 100000)
        payload = """{
            "RequestHeader":{
                "CFRequestId":"500653901",
                "RequestDate":"2019-06-26T23:05:41.2898238Z",
                "CFApiUserId":null,
                "CFApiPassword":null,
                "IsTestLead":true
            },
            "Business":{
                "Name":"Wow Inc",
                "SelfReportedCashFlow":{
                    "AnnualRevenue":49999999.0,
                    "MonthlyAverageBankBalance":900,
                    "MonthlyAverageCreditCardVolume":18191.0
                },
                "Address":{
                    "Address1":"1234 Red Ln",
                    "Address2":"5678 Blue Rd",
                    "City":"Santa Monica",
                    "State":"CA",
                    "Zip":"45321"
                },
                "TaxID":"839674398",
                "Phone":"6573248876",
                "NAICS":"79232",
                "HasBeenProfitable":false,
                "HasBankruptedInLast7Years":false,
                "InceptionDate":"2008-06-28T23:04:03.5507585+00:00"
            },
            "Owners":[],
            "CFApplicationData":{
                "RequestedLoanAmount":"10000",
                "StatedCreditHistory":1,
                "LegalEntityType":"LLC",
                "FilterID":"897079"
            }
        }"""
        resp = self.client.post('/loanapp/', payload, content_type='application/json')
        self.assertEqual(resp.status_code, 201)
        resp = self.client.get('/status/{}'.format(json.loads(resp.content)['id']))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(json.loads(resp.content)['status'], 'rejected')


# TODO Write tests for duplicate application handling
