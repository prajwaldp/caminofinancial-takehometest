from applications import models


def check_if_application_is_a_duplicate(application_data):
    """
    Returns (True, models.Application()) if the application_data matches
    an existing models.Application instance in the database, (False, None)
    otherwise

    The duplicate filter query is built on the following models.Application
    fields: ['CFApplicationData']['RequestedLoanAmount'],
    ['Business']['Name'], and ['Business']['TaxID']
    """

    requested_loan_amount = application_data['CFApplicationData']['RequestedLoanAmount']
    business_name = application_data['Business']['Name']
    business_tax_id = application_data['Business']['TaxID']

    queryset = models.Application.objects.filter(
        CFApplicationData__RequestedLoanAmount=requested_loan_amount,
        Business__Name=business_name,
        Business__TaxID=business_tax_id)

    if queryset.exists():
        if len(queryset) != 1:
            raise RuntimeError('More than 1 duplicate applications exist')
        return True, queryset[0]

    return False, None
