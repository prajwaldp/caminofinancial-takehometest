from django.http import JsonResponse, Http404
from rest_framework import viewsets
from applications import models, serializers, utils


class AddressViewSet(viewsets.ModelViewSet):
    queryset = models.Address.objects.all()
    serializer_class = serializers.AddressSerializer


class ApplicationViewSet(viewsets.ModelViewSet):
    queryset = models.Application.objects.all()
    serializer_class = serializers.ApplicationSerializer

    def perform_create(self, serializer):
        is_duplicate_application, original_application = \
            utils.check_if_application_is_a_duplicate(self.request.data)

        if is_duplicate_application:
            # Update the fields on the original application
            # and don't save the new instance
            serializer = serializers.ApplicationSerializer(
                original_application, data=self.request.data, partial=True)
            serializer.is_valid()
            serializer.save()
        else:
            serializer.save(data=self.request.data)

        return None


class BusinessViewSet(viewsets.ModelViewSet):
    queryset = models.Business.objects.all()
    serializer_class = serializers.BusinessSerializer


class PersonViewSet(viewsets.ModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer


def application_status(request, application_id):
    try:
        application = models.Application.objects.get(pk=application_id)
    except models.Application.DoesNotExist:
        raise Http404('application does not exist')

    if application.Business.HasBankruptedInLast7Years:
        return JsonResponse(dict(status='rejected'))

    # Cruel, I know. But this is just sample logic!
    if not application.Business.HasBeenProfitable:
        return JsonResponse(dict(status='rejected'))

    return JsonResponse(dict(status='pending'))
