from django.db import models


class RequestHeader(models.Model):
    CFRequestId = models.CharField(max_length=50)
    RequestDate = models.DateTimeField()
    CFApiUserId = models.CharField(max_length=50, null=True)
    CFApiPassword = models.CharField(max_length=50, null=True)
    IsTestLead = models.BooleanField()


class Address(models.Model):
    Address1 = models.CharField(max_length=50)
    Address2 = models.CharField(max_length=50, null=True)
    City = models.CharField(max_length=30)
    State = models.CharField(max_length=30)
    Zip = models.CharField(max_length=6)


class Person(models.Model):
    Name = models.CharField(max_length=50)
    FirstName = models.CharField(max_length=50)
    LastName = models.CharField(max_length=50)
    Email = models.EmailField()
    HomeAddress = models.ForeignKey(Address, on_delete=models.CASCADE)
    DateOfBirth = models.DateField()
    HomePhone = models.CharField(max_length=10)
    SSN = models.CharField(max_length=9)


class SelfReportedCashFlow(models.Model):
    AnnualRevenue = models.DecimalField(max_digits=15, decimal_places=1)
    MonthlyAverageBankBalance = models.DecimalField(max_digits=15, decimal_places=1)
    MonthlyAverageCreditCardVolume = models.DecimalField(max_digits=15, decimal_places=1)


class Business(models.Model):
    Name = models.CharField(max_length=50)
    SelfReportedCashFlow = models.ForeignKey(SelfReportedCashFlow, on_delete=models.CASCADE)
    Address = models.ForeignKey(Address, on_delete=models.CASCADE)
    TaxID = models.CharField(max_length=50)
    Phone = models.CharField(max_length=50)
    NAICS = models.CharField(max_length=50)
    HasBeenProfitable = models.BooleanField()
    HasBankruptedInLast7Years = models.BooleanField()
    InceptionDate = models.DateTimeField()


class CFApplicationData(models.Model):
    RequestedLoanAmount = models.CharField(max_length=20)
    StatedCreditHistory = models.IntegerField()
    LegalEntityType = models.CharField(max_length=20)
    FilterID = models.CharField(max_length=20)


class Application(models.Model):
    RequestHeader = models.ForeignKey(RequestHeader, on_delete=models.CASCADE)
    Business = models.ForeignKey(Business, on_delete=models.CASCADE)
    Owners = models.ManyToManyField(Person, through='Ownership')
    CFApplicationData = models.ForeignKey(CFApplicationData, on_delete=models.CASCADE)


class Ownership(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    percentage_of_ownership = models.DecimalField(decimal_places=2, max_digits=5)
